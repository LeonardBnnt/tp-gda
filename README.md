# TP GDA

-- Développement d'une application de gestion de bibliothèque --

Merci de créer une application qui serait le logiciel de gestion de livre d'une bibliothèque.

 

--- Points principaux de progression ---

- Préférer des commits restreints, clairs et nommés sensément. Merci de nous fournir l’URL publique du git dès que vous commencez.

- Essayer d'effectuer les modifications de base de données via des migrations doctrines https://symfony.com/doc/current/bundles/DoctrineMigrationsBundle/index.html

- Possibilité d'utiliser des data fixtures si le concept est maîtrisé

- Toute amélioration ou extension des features demandées

 

--- Tâches ---

- Créer une application symfony vierge (préférer l'utilisation de "composer create-project" avec le "website skeleton")

- Créer un controller de page d'accueil, ainsi qu'un template (+ layout) pour afficher un menu et un message d'accueil simple

- Ajouter webpack encore (côté PHP avec composer et côté js avec yarn) https://symfony.com/doc/current/frontend/encore/installation.html et https://symfony.com/doc/current/frontend.html

- Ajouter un fichier feuille de style au format ".scss", changer la couleur du background et du texte puis afficher ce style sur le site en utilisant "yarn build" / "yarn watch"

- Ajouter un fichier ".js" avec un simple console.log('hello world'), valider son exécution sur le site

- Ajouter bootstrap au scss, à installer en utilisant yarn

- Ajouter une gestion d'entité utilisateur en base de données https://symfony.com/doc/current/security.html

- Ajouter une commande symfony pour créer un utilisateur avec un rôle demandé, son login et son mot de passe lors de la création

- Ajouter une entité livre Book[title, slug, isbn, resume, price] avec son CRUD

- Ajouter une entité éditeur Editor[name, foundation year] avec son CRUD (un livre a 0 ou 1 éditeur)

- Ajouter une entité auteur Author[firstname, lastname, birthday] avec son CRUD (un livre a 1 auteur)

- Ajouter une entité catégorie Category[name, slug] avec son CRUD (un livre à 1 ou plusieurs catégorie(s))

- Les CRUD de ces entités ne doivent à présent être accessibles que par un utilisateur qui possède un rôle "ROLE_ADMIN" dans son espace compte

- Création d'un moteur de recherche sur l'accueil, capable de chercher dans les title et isbn des livres en utilisant un champ texte

- Modification de ces champs de recherche pour pouvoir filtrer les résultats par prix en utilisant des opérateurs de comparaison du type >10 ou <17, donc une recherche de "bob <60" ne renverrait que les livres comportant bob dans leur titre ET possédant un prix inférieur à 60

- Donner la possibilité aux utilisateurs "classiques" de liker un livre et de pouvoir en consulter la liste dans son espace compte

- Toute autre idée/amélioration est la bienvenue
