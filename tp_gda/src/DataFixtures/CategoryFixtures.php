<?php

namespace App\DataFixtures;

use App\Entity\Category;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class CategoryFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        for ($i = 0; $i < 2; $i++) {
            $category = new Category();
            $category->setName('Category '.$i);
            $category->setSlug('category-'.$i);

            $this->addReference('CATEGORY'.$i, $category);

            $manager->persist($category);
        }

        $manager->flush();
    }
}
