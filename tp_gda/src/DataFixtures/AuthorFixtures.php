<?php

namespace App\DataFixtures;

use App\Entity\Author;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AuthorFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        for ($i = 0; $i < 3; $i++) {
            $author = new Author();
            $author->setFirstname('Prenom '.$i);
            $author->setLastname('Nom '.$i);
            $author->setBirthday(new \DateTime('now'));

            $this->addReference('AUTHOR'.$i, $author);

            $manager->persist($author);
        }

        $manager->flush();
    }
}
