<?php

namespace App\DataFixtures;

use App\Entity\Editor;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class EditorFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        for ($i = 0; $i < 2; $i++) {
            $editor = new Editor();
            $editor->setName('Editor '.$i);
            $editor->setFoundationYear($i);

            $editor->addBook(
                $this->getReference('LIVRE'.$i)
            );

            $manager->persist($editor);
        }

        $manager->flush();
    }

    public function getDependencies(): array
    {
        return array(
            BookFixtures::class,
        );
    }
}
