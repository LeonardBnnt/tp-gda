<?php

namespace App\DataFixtures;

use App\Entity\Book;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class BookFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        for ($i = 0; $i < 5; $i++) {
            $livre = new Book();
            $livre->setTitle('Livre '.$i);
            $livre->setSlug('livre-'.$i);
            $livre->setIsbn('i-sbn-livre-'.$i);
            $livre->setPrice($i);
            $livre->setResume('This is the resume for book n°'.$i);

            $livre->setAuthor(
                $this->getReference('AUTHOR2')
            );
            $livre->addCategory(
                $this->getReference('CATEGORY0')
            );
            $livre->addCategory(
                $this->getReference('CATEGORY1')
            );

            $this->addReference('LIVRE'.$i, $livre);

            $manager->persist($livre);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return array(
            AuthorFixtures::class,
            CategoryFixtures::class,
        );
    }
}
